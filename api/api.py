import os
from flask import Flask
from flask import jsonify, render_template, redirect, request, url_for, json
import numpy as np
import pandas as pd
app = Flask(__name__)
ROOT_DIR = os.path.abspath('.')
ALLOWED_EXTENSIONS = {'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'}
UPLOAD_DIR = os.path.join(ROOT_DIR,'static','images','upload')
def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS
def get_current_index():
    try:
        image_path = os.path(UPLOAD_DIR)
        image_path = image_path.glob('.')
        image_name = [path.name for path in image_path]
        image_name = [int(name.split('.')[0]) for name in image_name]
        current_index = max(image_name)
    except: 
        current_index = 0
    return current_index

global current_index
current_index = get_current_index()

@app.route('/image',methods=['GET','POST', 'OPTIONS'])
def api():
    print(request.method)
    print(request.files)
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            print('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            print('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            global current_index 
            current_index += 1 
            file.save(os.path.join(UPLOAD_DIR, str(current_index)+".jpg"))
            new_filename = str(current_index)+".jpg" 
    return json_response({'ok': 'file uploaded'})
def json_response(payload, status=200):
 return (json.dumps(payload), status, {'content-type': 'application/json'})